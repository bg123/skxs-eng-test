; ===========================================================================
; Place macros here
; ===========================================================================

doBankswitch: MACRO
	rst $20
ENDM

scr_face: MACRO
; make ballot face X
	db $05, \1
ENDM
scr_text: MACRO
	db $14
	dw \1
ENDM
scr_unkn:	MACRO
	db $0F, \1, \2
ENDM

tx_char:	MACRO
; char, name, expression
	db $e0, \1, \2
ENDM

tx:	MACRO
	db $F0, \1
ENDM

tx_cont:	MACRO
	db $EE, \1
ENDM

tx_para:	MACRO
	db $EC, \1
ENDM

tx_line:	MACRO
	db $ED, \1
ENDM

tx_end:	MACRO
	db $E2
ENDM

; ===========================================================================
; Place constants includes here
; ===========================================================================

INCLUDE "./constants/hardware.asm"

W_BASE		EQU $C000
wStack		EQU $DFFF
H_BASE		EQU $FF80
H_TargetBank		EQU $FFBE

charmap " ", $00
charmap "A", $01
charmap "B", $02
charmap "C", $03
charmap "D", $04
charmap "E", $05
charmap "F", $06
charmap "G", $07
charmap "H", $08
charmap "I", $09
charmap "J", $0A
charmap "K", $0B
charmap "L", $0C
charmap "M", $0D
charmap "N", $0E
charmap "O", $0F
charmap "P", $10
charmap "Q", $11
charmap "R", $12
charmap "S", $13
charmap "T", $14
charmap "U", $15
charmap "V", $16
charmap "W", $17
charmap "X", $18
charmap "Y", $19
charmap "Z", $1A
charmap "(", $1B
charmap ")", $1C
charmap ":", $1D
charmap ";", $1E
charmap "[", $1F
charmap "]", $20
charmap "a", $21
charmap "b", $22
charmap "c", $23
charmap "d", $24
charmap "e", $25
charmap "f", $26
charmap "g", $27
charmap "h", $28
charmap "i", $29
charmap "j", $2A
charmap "k", $2B
charmap "l", $2C
charmap "m", $2D
charmap "n", $2E
charmap "o", $2F
charmap "p", $30
charmap "q", $31
charmap "r", $32
charmap "s", $33
charmap "t", $34
charmap "u", $35
charmap "v", $36
charmap "w", $37
charmap "x", $38
charmap "y", $39
charmap "z", $3A
charmap "'d", $3B
charmap "'l", $3C
charmap "'s", $3D
charmap "'t", $3E
charmap "'v", $3F
charmap "0", $40
charmap "1", $41
charmap "2", $42
charmap "3", $43
charmap "4", $44
charmap "5", $45
charmap "6", $46
charmap "7", $47
charmap "8", $48
charmap "9", $49
charmap "?", $4a
charmap "!", $4b
charmap ".", $4d
charmap "/", $4e
charmap "<comma>", $4f
charmap "Ａ",$50
charmap "Ｂ",$51
charmap "Ｃ",$52
charmap "Ｄ",$53
charmap "Ｅ",$54
charmap "Ｆ",$55
charmap "Ｇ",$56
charmap "Ｈ",$57
charmap "Ｉ",$58
charmap "Ｊ",$59
charmap "Ｋ",$5a
charmap "Ｌ",$5b
charmap "Ｍ",$5c
charmap "Ｎ",$5d
charmap "Ｏ",$5e
charmap "Ｐ",$5f
charmap "Ｑ",$60
charmap "Ｒ",$61
charmap "Ｓ",$62
charmap "Ｔ",$63
charmap "Ｕ",$64
charmap "Ｖ",$65
charmap "Ｗ",$66
charmap "Ｘ",$67
charmap "Ｙ",$68
charmap "Ｚ",$69

charmap "ａ",$6a
charmap "ｂ",$6b
charmap "ｃ",$6c
charmap "ｄ",$6d
charmap "ｅ",$6e
charmap "ｆ",$6f
charmap "ｇ",$70
charmap "ｈ",$71
charmap "ｉ",$72
charmap "ｊ",$73
charmap "ｋ",$74
charmap "ｌ",$75
charmap "ｍ",$76
charmap "ｎ",$77
charmap "ｏ",$78
charmap "ｐ",$79
charmap "ｑ",$7a
charmap "ｒ",$7b
charmap "ｓ",$7c
charmap "ｔ",$7d
charmap "ｕ",$7e
charmap "ｖ",$7f
charmap "ｗ",$80
charmap "ｘ",$81
charmap "ｙ",$82
charmap "ｚ",$83
charmap "'m",$84

; ===========================================================================
; Routines used by the game
; ===========================================================================

ScrFadeIn	EQU	$398
LoadToVRAM	EQU	$354
FillVRAM	EQU	$1063

Copy_SrcPtrs	EQU	$d9c0
Copy_DesPtrs	EQU	$d0f6
Copy_NumTiles	EQU $cbf2
CopyMap_isReady		EQU	$cbf4
Copy_isReady		EQU	$cbf5

; ===========================================================================
; Place RAM variables here
; ===========================================================================

w_IsSign	EQU	$d008
; ---------------------------------------------------------------------------
; WRAM
; ---------------------------------------------------------------------------
SECTION "WRAM Bank 0", WRAM0
w_Start::		ds 1		; beginning of WRAM
	
; ---------------------------------------------------------------------------
; HRAM
; ---------------------------------------------------------------------------
SECTION "Hram", HRAM
hRAMStart:		ds 1		; beginning of HRAM
