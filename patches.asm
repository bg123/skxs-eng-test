INCLUDE "./constants.asm"

INCLUDE "./patches/main_functions.asm"
;INCLUDE "./patches/bypass_save.asm"
INCLUDE "./patches/names.asm"
INCLUDE "./patches/main_text_engine.asm"
INCLUDE "./patches/intro.asm"
INCLUDE "./patches/title.asm"

INCLUDE "./patches/scripts/ballots_home.asm"
INCLUDE "./patches/scripts/other_text.asm"

;5123 = start menu?

; 8:59dd=8:55e4, overwritten by outside observatory text
