Func_2921: ; 2921 (0:2921)
	ld a, [$cbf5]
	and a
	ret z
	ld a, [$7fff]
	push af
	ld a, [$ffbe]
	rst $20
	ld [$ffa2], sp
	ld a, [$d9c0]
	ld l, a
	ld a, [$d9c1]
	ld h, a
	ld sp, [hl]
	ld a, [$d0f6]
	ld l, a
	ld a, [$d0f7]
	ld h, a
	ld a, [$cbf2]
	ld e, a
.asm_2945
	pop bc
	ld [hl], c
	inc l
	ld [hl], c
	inc l
	ld [hl], b
	inc l
	ld [hl], b
	inc l
	pop bc
	ld [hl], c
	inc l
	ld [hl], c
	inc l
	ld [hl], b
	inc l
	ld [hl], b
	inc l
	pop bc
	ld [hl], c
	inc l
	ld [hl], c
	inc l
	ld [hl], b
	inc l
	ld [hl], b
	inc l
	pop bc
	ld [hl], c
	inc l
	ld [hl], c
	inc l
	ld [hl], b
	inc l
	ld [hl], b
	inc hl
	dec e
	jr nz, .asm_2945
	ld a, [$ffa2]
	ld l, a
	ld a, [$ffa3]
	ld h, a
	ld sp, [hl]
	xor a
	ld [$cbf5], a
	pop af
	rst $20
	ret
; 0x297a
Func_2bf: ; 2bf (0:2bf)
.asm_2bf
	ld a, [$ff40]
	and a
	ret z
	ld a, [$ff90]
	and a
	jr z, .asm_2bf
	xor a
	ld [$ff90], a
	ret
; 0x2cc
Func_19e0: ; 19e0 (0:19e0)
	ret
; 0x19e1
