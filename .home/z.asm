Func_1a2b: ; 1a2b (0:1a2b)
	ld [$db1e], a
	call Func_1aa4
.asm_1a31
	ld a, [$ff95]
	bit 0, a
	jr z, .asm_1a3c
	ld a, $01
	ld [$d085], a
.asm_1a3c
	call Func_2bf
	ld a, [$d085]
	dec a
	ld [$d085], a
	jr nz, .asm_1a31
	call Func_1a5f
	ld a, $01
	ld [$cbf4], a
	call Func_2bf
	ld hl, $cbf0
	inc [hl]
	ld a, $03
	ld [$d085], a
	jp Func_19f8
; 0x1a5f
Func_1a0e: ; 1a0e (0:1a0e)
	and $0f
	push af
	srl a
	add $40
	ld [$ffbe], a
	pop af
	bit 0, a
	jr nz, .asm_1a21
	ld a, $40
	jr .asm_1a23
.asm_1a21
	ld a, $60
.asm_1a23
	ld [$dcd2], a
	xor a
	ld [$dcd1], a
	ret
; 0x1a2b
Func_1a5f: ; 1a5f (0:1a5f)
	ld hl, $50d
	ld a, [$dce3]
	and a
	jr z, .asm_1a6b
	ld hl, $503
.asm_1a6b
	ld a, [$cbf1]
	add a
	add l
	ld l, a
	ld a, [$cbf0]
	add a
	add h
	ld h, a
	call Func_7b1
	ld e, l
	ld d, h
	ld hl, $c0b0
	push de
	call Func_1a8e
	pop de
	ld a, e
	inc a
	and $1f
	ld b, a
	ld a, e
	and $e0
	or b
	ld e, a
	ld a, e
	ld [hli], a
	ld a, d
	ld [hli], a
	ld a, $20
	add e
	ld e, a
	jr nc, .asm_1a9f
	inc d
	ld a, d
	and $03
	or $98
	ld d, a
.asm_1a9f
	ld a, e
	ld [hli], a
	ld a, d
	ld [hli], a
	ret
; 0x1aa4
Func_1aa4: ; 1aa4 (0:1aa4)
	ld a, [$dce0]
	add $a8
	ld c, a
	ld [$cbf3], a
	swap a
	ld b, a
	and $0f
	or $80
	ld [$d0f7], a
	ld a, b
	and $f0
	ld [$d0f6], a
	ld a, [$d0cf]
	ld l, a
	ld a, [$d0d0]
	ld h, a
	ld de, $41
	add hl, de
	ld a, [$cbf0]
	add a
	ld e, a
	ld a, [$cbf1]
	and a
	jr z, .asm_1ad8
	ld a, $28
	add e
	ld e, a
.asm_1ad8
	ld d, $00
	add hl, de
	ld de, $14
	push hl
	ld [hl], c
	inc c
	add hl, de
	ld [hl], c
	inc c
	pop hl
	inc hl
	ld [hl], c
	inc c
	add hl, de
	ld [hl], c
	ld a, [$dcd1]
	ld e, a
	ld a, [$dcd2]
	ld d, a
	ld a, [$db1e]
	ld l, a
	ld h, $00
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, de
	ld a, l
	ld [$d9c0], a
	ld a, h
	ld [$d9c1], a
	ld a, $04
	ld [$cbf2], a
	ld a, $01
	ld [$cbf5], a
	call Func_2bf
	ld a, [$dce0]
	add $04
	ld [$dce0], a
	cp $38
	ret c
	xor a
	ld [$dce0], a
	ret
; 0x1b23
Func_1a8e: ; 1a8e (0:1a8e)
	ld a, e
	ld [hli], a
	ld a, d
	ld [hli], a
	ld a, $20
	add e
	ld e, a
	jr nc, .asm_1a9f
	inc d
	ld a, d
	and $03
	or $98
	ld d, a
.asm_1a9f
	ld a, e
	ld [hli], a
	ld a, d
	ld [hli], a
	ret
; 0x1aa4
Func_28fb: ; 28fb (0:28fb)
	ld a, [$cbf4]
	and a
	ret z
	ld [$ffa2], sp
	ld hl, $c0b0
	ld sp, [hl]
	ld a, [$cbf3]
	pop bc
	ld [bc], a
	inc a
	pop bc
	ld [bc], a
	inc a
	pop bc
	ld [bc], a
	inc a
	pop bc
	ld [bc], a
	ld a, [$ffa2]
	ld l, a
	ld a, [$ffa3]
	ld h, a
	ld sp, [hl]
	xor a
	ld [$cbf4], a
	ret
; 0x2921
Func_7b1: ; 7b1 (0:7b1)
	push bc
	push de
	ld bc, $9800
	ld e, h
	ld a, l
	add a
	add a
	add a
	ld l, a
	ld a, [$ffb0]
	add l
	ld l, a
	ld h, $00
	add hl, hl
	add hl, hl
	ld a, e
	add a
	add a
	add a
	ld e, a
	ld a, [$ffae]
	add e
	srl a
	srl a
	srl a
	ld e, a
	ld d, $00
	add hl, de
	add hl, bc
	ld a, l
	ld [$dace], a
	ld a, h
	ld [$dacf], a
	pop de
	pop bc
	ret
; 0x7e2
