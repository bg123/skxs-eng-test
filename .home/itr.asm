Func_1dc043: ; 1dc043 (77:4043)
	ld hl, $9800
	ld de, $6e97
	ld bc, $1412
	ld a, $12
	ld [$ff93], a
	ld a, $14
	ld [$ff92], a
	call Func_348
	ld hl, $7167
	ld de, $cab0
	ld bc, $40
	call Func_b30
	ld hl, $7177
	ld de, $9000
	ld bc, $3e0
	call Func_2f7
	ld a, $c7
	ld [$ff40], a
	ld hl, $cab0
	xor a
	ld [$ffc4], a
	ld [$ff9d], a
	call Func_398
	call Func_1dc311
	ld bc, $cab0
	xor a
	ld [$ffc4], a
	call Func_92e
	call Func_9d2
	ld a, $6d
	call Func_2be2
	ld hl, $9800
	ld de, $5577
	ld bc, $1412
	ld a, $12
	ld [$ff93], a
	ld a, $14
	ld [$ff92], a
	call Func_354
	ld hl, $9800
	ld de, $540f
	ld bc, $1412
	ld a, $12
	ld [$ff93], a
	ld a, $14
	ld [$ff92], a
	call Func_348
	ld hl, $5337
	ld de, $cab0
	ld bc, $40
	call Func_b30
	ld hl, $537f
	ld de, $caf0
	ld bc, $40
	call Func_b30
	ld hl, $56df
	ld de, $9000
	ld bc, $170
	call Func_2f7
	ld hl, $584f
	ld de, $8000
	ld bc, $400
	call Func_2f7
	call Func_1dd2a6
	ld a, $c7
	ld [$ff40], a
	ld hl, $cab0
	xor a
	ld [$ffc4], a
	ld [$ff9d], a
	call Func_398
	call Func_2bf
	call Func_1dcd06
	ld a, [$ff9d]
	inc a
	ld [$ff9d], a
	ld a, [$ffbf]
	and a
	jr nz, .asm_1dc118
	call Func_1dd23f
	call Func_1dcb70
	call Func_1dcbf8
	jp Func_1dc0fc
.asm_1dc118
	ld bc, $cab0
	xor a
	ld [$ffc4], a
	call Func_92e
	call Func_9d2
	call Func_1dd289
	call Func_1dd297
	ld a, $6d
	call Func_2be2
	xor a
	ld [$ffbf], a
	ld [$ffae], a
	ld [$ffaf], a
	ld [$ffb0], a
	ld [$ffb1], a
	ld [$dcf3], a
	ld [$dcf4], a
	ld [$dcfb], a
	ld [$dce8], a
	ld [$dcf5], a
	call Func_1dc943
	call Func_1dd2a6
	ld a, $e7
	ld [$ff40], a
	ld hl, $cab0
	xor a
	ld [$ffc4], a
	ld [$ff9d], a
	call Func_398
	call Func_2bf
	call Func_1dce10
	ld a, [$ff9d]
	inc a
	ld [$ff9d], a
	ld a, [$ffbf]
	and a
	jr nz, .asm_1dc18a
	call Func_1dd23f
	ld a, [$dcf5]
	cp $01
	jr z, .asm_1dc196
	cp $02
	jr z, .asm_1dc1a2
	cp $03
	jr z, .asm_1dc1f1
	cp $04
	jr z, .asm_1dc19c
	call Func_1dc856
	jp Func_1dc15e
.asm_1dc18a
	ld bc, $cab0
	xor a
	ld [$ffc4], a
	call Func_96a
	jp Func_1dc1fc
.asm_1dc196
	call Func_1dc8d9
	jp Func_1dc15e
.asm_1dc19c
	call Func_1dc8bc
	jp Func_1dc15e
.asm_1dc1a2
	ld a, [$dce8]
	inc a
	ld [$dce8], a
	cp $08
	jr c, .asm_1dc1be
	xor a
	ld [$dce8], a
	ld a, $01
	ld [$ffbf], a
	ld c, $40
	call Func_1dd282
	jp Func_1dc15e
.asm_1dc1be
	ld c, $40
	call Func_1dd282
	ld a, $00
	ld [$dcf5], a
	ld bc, $cab0
	xor a
	ld [$ffc4], a
	call Func_96a
	call Func_9d2
	call Func_1dd2a6
	xor a
	ld [$ffbf], a
	ld [$ffae], a
	ld [$ffaf], a
	ld [$ffb0], a
	ld [$ffb1], a
	ld [$dcf3], a
	ld [$dcf4], a
	ld [$dcfb], a
	ld [$dcf5], a
	jp Func_1dc149
.asm_1dc1f1
	call Func_1dc943
	ld a, $01
	ld [$dcf5], a
	jp Func_1dc15e
; 0x1dc1fc
Func_7e2: ; 7e2 (0:7e2)
	push hl
	ld de, $d100
	ld bc, $1408
	ld a, b
	ld [$ff92], a
	ld a, c
	ld [$ff93], a
	call Func_348
	pop hl
	ld a, [$d0cf]
	ld e, a
	ld a, [$d0d0]
	ld d, a
	ld bc, $1408
	ld a, b
	ld [$ff92], a
	ld a, c
	ld [$ff93], a
	call Func_358
	ret
; 0x80a
