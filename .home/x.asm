Func_1ba0: ; 1ba0 (0:1ba0)
	ld bc, $100
	ld hl, $8e00
	xor a
	call Func_1063
	call Func_2bf
	ld a, $0a
	rst $20
	xor a
	ld [$dce0], a
	ld de, $45fb
	ld a, [$cbf7]
	ld l, a
	ld h, $00
	add hl, hl
	add hl, de
	ld a, [hli]
	ld h, [hl]
	ld l, a
	ld a, $0a
	rst $20
	ld a, [hli]
	push hl
	cp $f0
	jr nc, .asm_1c20
	cp $ed
	jr z, .asm_1c27
	ld [$db1e], a
	ld a, [$dce0]
	add $e0
	ld c, a
	ld [$cbf3], a
	swap a
	ld b, a
	and $0f
	or $80
	ld [$d0f7], a
	ld a, b
	and $f0
	ld [$d0f6], a
	ld a, [$dcd1]
	ld e, a
	ld a, [$dcd2]
	ld d, a
	ld a, [$db1e]
	ld l, a
	ld h, $00
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, de
	ld a, l
	ld [$d9c0], a
	ld a, h
	ld [$d9c1], a
	ld a, $04
	ld [$cbf2], a
	ld a, $01
	ld [$cbf5], a
	call Func_2bf
	ld a, [$dce0]
	add $04
	ld [$dce0], a
	pop hl
	jp Func_1bc2
.asm_1c20
	call Func_1a0e
	pop hl
	jp Func_1bc2
.asm_1c27
	pop hl
	ld a, $0a
	rst $20
	ret
; 0x1c2c
Func_1ba0: ; 1ba0 (0:1ba0)
	ld bc, $100
	ld hl, $8e00
	xor a
	call Func_1063
	call Func_2bf
	ld a, $0a
	rst $20
	xor a
	ld [$dce0], a
	ld de, $45fb
	ld a, [$cbf7]
	ld l, a
	ld h, $00
	add hl, hl
	add hl, de
	ld a, [hli]
	ld h, [hl]
	ld l, a
	ld a, $0a
	rst $20
	ld a, [hli]
	push hl
	cp $f0
	jr nc, .asm_1c20
	cp $ed
	jr z, .asm_1c27
	ld [$db1e], a
	ld a, [$dce0]
	add $e0
	ld c, a
	ld [$cbf3], a
	swap a
	ld b, a
	and $0f
	or $80
	ld [$d0f7], a
	ld a, b
	and $f0
	ld [$d0f6], a
	ld a, [$dcd1]
	ld e, a
	ld a, [$dcd2]
	ld d, a
	ld a, [$db1e]
	ld l, a
	ld h, $00
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, de
	ld a, l
	ld [$d9c0], a
	ld a, h
	ld [$d9c1], a
	ld a, $04
	ld [$cbf2], a
	ld a, $01
	ld [$cbf5], a
	call Func_2bf
	ld a, [$dce0]
	add $04
	ld [$dce0], a
	pop hl
	jp Func_1bc2
.asm_1c20
	call Func_1a0e
	pop hl
	jp Func_1bc2
.asm_1c27
	pop hl
	ld a, $0a
	rst $20
	ret
; 0x1c2c
