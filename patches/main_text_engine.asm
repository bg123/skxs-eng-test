SECTION "Fix_SM", ROM0[$c8d]
; This is called by the start menu
	call ChkText_MoveBank

SECTION "VB_Ptrs", ROM0[$286a]
	call Func_2921
	call Func_28fb

SECTION "Tiles_Update", ROM0[$28fb]
Func_28fb:: ; 28fb (0:28fb)
; Update tilemaps
; this arranges tiles vertically
; mainly for chinese text
	ld a, [CopyMap_isReady]
	and a
	ret z
	ld [$ffa2], sp
	ld hl, $c0b0
	ld sp, hl
	ld a, [$cbf3]
	pop bc
	ld [bc], a
;	inc a		;
;	pop bc		;
;	ld [bc], a		;
;	inc a		;
;	pop bc		;
;	ld [bc], a		;
;	inc a		;
;	pop bc		;
;	ld [bc], a		;
	ld a, [$ffa2]
	ld l, a
	ld a, [$ffa3]
	ld h, a
	ld sp, hl
	xor a
	ld [CopyMap_isReady], a
	ret
Func_2921:: ; 2921 (0:2921)
; This updates the tilesets
; for text
	ld a, [Copy_isReady]
	and a
	ret z			; If not making requests right now
	ld a, [$7fff]
	push af			; save current bank number
	ld a, [H_TargetBank]
	doBankswitch
	ld [$ffa2], sp	; store SP
	ld a, [Copy_SrcPtrs]
	ld l, a
	ld a, [Copy_SrcPtrs+1]
	ld h, a			; hl = tile pointer
	ld sp, hl
	ld a, [Copy_DesPtrs]
	ld l, a
	ld a, [Copy_DesPtrs+1]
	ld h, a			; hl = destination
	ld a, [Copy_NumTiles]
	ld e, a			; # tiles to update
.asm_2945
	pop bc
	ld [hl], c
	inc l
	ld [hl], c
	inc l
	ld [hl], b
	inc l
	ld [hl], b
	inc l
	pop bc
	ld [hl], c
	inc l
	ld [hl], c
	inc l
	ld [hl], b
	inc l
	ld [hl], b
	inc l
	pop bc
	ld [hl], c
	inc l
	ld [hl], c
	inc l
	ld [hl], b
	inc l
	ld [hl], b
	inc l
	pop bc
	ld [hl], c
	inc l
	ld [hl], c
	inc l
	ld [hl], b
	inc l
	ld [hl], b
	inc hl
	dec e
	jr nz, .asm_2945
	ld a, [$ffa2]
	ld l, a
	ld a, [$ffa3]
	ld h, a
	ld sp, hl
	xor a
	ld [Copy_isReady], a
	pop af
	rst $20
	ret
; 0x297a

SECTION "Text_Engine0", ROM0[$19CA]
Func_19ca::
   ldh a,[$FFBC]	; text type?
   cp a,$01
   jr z,.type1
   cp a,$02
   jr z,.type2
   cp a,$03
   jr z,.type3
   cp a,$04
   jr z,.type4
   cp a,$05
   jr z,.type5
   ret 
.type1
	ldh  a,[$FFB6]	; switch to bank #
	jr   .common
.type2
	ldh  a,[$FFC0]
	jr   .common
.type3
	ldh  a,[$FFC3]
	jr   .common
.type4
	ldh  a,[$FFB7]
	jr   .common
.type5
	ld   a,[$DCB5]
	jr   .common
.common
   doBankswitch
   push hl
   
ChkText::	; 19f8
   pop hl
   ld a,[hli]
   push hl
   cp a,$F0
   jp nc,.isCharacterSet
   cp a, $e2
   jr z, .unsetSign
   cp a, $e1
   jr z, .setSign
   cp a,$E0
   jp nc,$1b23
   jp ChkText_Continue
   
.isCharacterSet
   call ChkText_MoveBank
   jp ChkText
   
.setSign
	ld a, 1
	ld [w_IsSign], a
	jp $1c2c
	
.unsetSign
	push af
	xor a
	ld [w_IsSign], a
	pop af
	jp $1c6e

ChkText_MoveBank:
;	and $0f
	push af
;	srl a
;	add $40
	ld a, $3f
	ld [H_TargetBank], a
	pop af
	bit 0, a
	jr nz, .asm_1a21
	ld a, $40
	jr .asm_1a23
.asm_1a21
	ld a, $60
.asm_1a23
	ld [$dcd2], a
	xor a
	ld [$dcd1], a
	ret
	
ChkText_Continue:	; 1a2b
	ld [$db1e], a		; current scanned byte is here
	call Asm1aa4		; prepare tileset placement
.asm_1a31
	ld a, [$ff95]
	bit 0, a
	jr z, .asm_1a3c
	ld a, $01
	ld [$d085], a
.asm_1a3c
;	call DelayFrame
	ld a, [$d085]
	dec a
	ld [$d085], a
	jr nz, .asm_1a31
	call .asm_1a5f
	ld a, $01
	ld [CopyMap_isReady], a
	call DelayFrame
	ld hl, $cbf0
	inc [hl]
	ld a, $03
	ld [$d085], a
	jp ChkText
; 0x1a5f
.asm_1a5f
	jp TextPosPatcharound
C1a5f_cont:
	ld a, [$cbf1]
	add a
	add l
	ld l, a
	ld a, [$cbf0]
;	add a
	inc a
	add h
	ld h, a
	call $7b1
	ld e, l
	ld d, h
	ld hl, $c0b0
	push de
	call .asm_1a8e
	pop de
	ld a, e
	inc a
	and $1f
	ld b, a
	ld a, e
	and $e0
	or b
	ld e, a
.asm_1a8e
	ld a, e
	ld [hli], a
	ld a, d
	ld [hli], a
	ld a, $20
	add e
	ld e, a
	jr nc, .asm_1a9f
	inc d
	ld a, d
	and $03
	or $98
	ld d, a
.asm_1a9f
	ld a, e
	ld [hli], a
	ld a, d
	ld [hli], a
	ret
; 0x1aa4
Asm1aa4:
	ld a, [$dce0]	; text position
	add $a8
	ld c, a
	ld [$cbf3], a
	swap a
	ld b, a
	and $0f
	or $80
	ld [$d0f7], a
	ld a, b
	and $f0
	ld [$d0f6], a
	ld a, [$d0cf]
	ld l, a
	ld a, [$d0d0]
	ld h, a
	ld de, $41
	add hl, de
	ld a, [$cbf0]
;	add a
	ld e, a
	ld a, [$cbf1]
	and a
	jr z, .asm_1ad8
	ld a, $28
	add e
	ld e, a
.asm_1ad8
	ld d, $00
	add hl, de
	ld de, $14
	push hl
	inc hl
	ld [hl], c	;mirror text
;	inc c
;	add hl, de
;	ld [hl], c	
;	inc c
	pop hl
;	inc hl
;	ld [hl], c
;	inc c
;	add hl, de
;	ld [hl], c
;-
;	ld a, [$dcd1]
;	ld e, a
;	ld a, [$dcd2]
;	ld d, a
	ld a, [$db1e]
	ld l, a
	ld h, $00
	ld de, Latin_Set
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, de
	ld a, l
	ld [Copy_SrcPtrs], a
	ld a, h
	ld [Copy_SrcPtrs+1], a
	ld a, $01
	ld [Copy_NumTiles], a
	ld a, $01
	ld [Copy_isReady], a
	call DelayFrame
	ld a, [$dce0]
	add $01
	ld [$dce0], a
	cp $38
	ret c
	xor a
	ld [$dce0], a
	ret
; 0x1b23

SECTION "Haxx0", ROM0[$3000]
TextPosPatcharound::
	ld a, [w_IsSign]
	and a
	jr z, .notSign
	ld hl, $10d
	ld a, [$dce3]
	and a
	jr z, .cont
	ld hl, $103
	jr .cont
.notSign
	ld hl, $50d					; TEXT START LOCATION
	ld a, [$dce3]				; up / down?
	and a
	jr z, .cont
	ld hl, $503
.cont
	jp C1a5f_cont

SECTION "Text_Engine1", ROM0[$1BA0]
Func_1ba0:: ; 1ba0 (0:1ba0)
	ld bc, $100
	ld hl, $8e00
	xor a
	call FillVRAM
	call DelayFrame
	
	ld a, $0a
	doBankswitch
	
	xor a
	ld [$dce0], a
	ld de, $45fb
	ld a, [$cbf7]
	ld l, a
	ld h, $00
	add hl, hl
	add hl, de
	ld a, [hli]
	ld h, [hl]
	ld l, a
	
Func_1bc2:: ; for name entry
	ld a, $0a
	doBankswitch
	ld a, [hli]
	push hl
;	cp $f0
;	jr nc, .asm_1c20
	cp $ed
	jr z, .asm_1c27			; Jump if end
	ld [$db1e], a
	ld a, [$dce0]
	add $e0
	ld c, a
	ld [$cbf3], a
	swap a
	ld b, a
	and $0f
	or $80
	ld [Copy_DesPtrs+1], a
	ld a, b
	and $f0
	ld [Copy_DesPtrs], a
;	ld a, [$dcd1]
;	ld e, a
;	ld a, [$dcd2]
;	ld d, a
	ld a, $3f
	ld [H_TargetBank], a
	ld de, Latin_Set
	ld a, [$db1e]
	ld l, a
	ld h, $00
	add hl, hl
	add hl, hl
	add hl, hl
	add hl, de
	ld a, l
	ld [Copy_SrcPtrs], a
	ld a, h
	ld [Copy_SrcPtrs+1], a	; index?????
	ld a, $01
	ld [Copy_NumTiles], a	; How many tiles to update
	ld a, $01
	ld [Copy_isReady], a	; request flag
	call DelayFrame
	ld a, [$dce0]
	inc a
	ld [$dce0], a
	pop hl
	jp Func_1bc2
;.asm_1c20
;	call $1a0e
;	pop hl
;	jp Func_1bc2
.asm_1c27
	pop hl
	ld a, $0a
	doBankswitch
	ret
; 0x1c2c
SECTION "LatinSet", ROMX[$6930], BANK[$3F]
Latin_Set:
	INCBIN "gfx/latinset.1bpp"
	
SECTION "NoticeSign", ROMX[$4423], BANK[$0a]
NoticeSign:
	INCBIN "gfx/noticesign.2bpp"
	
SECTION "NoticeFix", ROMX[$4398], BANK[$A]
	db $e0,$e1,$e2,$e3,$e4,$a0,$a0,$a0
	rept 10
	db $a0
	endr
	db $a4,$a4
	rept 8
	db $a0
	endr
