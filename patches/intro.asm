SECTION "Intro_Rtns", ROMX[$4043], BANK[$77]
Func_1dc043:: ; 1dc043 (77:4043)
	ld hl, vBGMap0
	ld de, $6e97
	ld bc, $1412
	ld a, $12
	ld [$ff93], a
	ld a, $14
	ld [$ff92], a
	call $348		;
	ld hl, $7167
	ld de, $cab0
	ld bc, $40
	call $b30		;
	ld hl, $7177
	ld de, $9000
	ld bc, $3e0
	call $2f7		;
	ld a, $c7
	ld [$ff40], a
	ld hl, $cab0
	xor a
	ld [$ffc4], a
	ld [$ff9d], a
	call ScrFadeIn		; screen fade in
	call $4311
	ld bc, $cab0
	xor a
	ld [$ffc4], a
	call $92e
	call $9d2
	ld a, $6d
	call $2be2
; Map
	ld hl, $9800
	ld de, IntroMap00
	ld bc, $1412
	ld a, $12
	ld [$ff93], a
	ld a, $14
	ld [$ff92], a
	call LoadToVRAM
	ld hl, $9800
	ld de, $540f
	ld bc, $1412
	ld a, $12
	ld [$ff93], a
	ld a, $14
	ld [$ff92], a
	call $348
	ld hl, $5337
	ld de, $cab0
	ld bc, $40
	call $b30
	ld hl, $537f
	ld de, $caf0
	ld bc, $40
	call $b30
	ld hl, $56df
	ld de, $9000
	ld bc, $170
	call $2f7
	ld hl, $584f
	ld de, $8000
	ld bc, $400
	call $2f7
	call $52ac
	ld a, $c7
	ld [$ff40], a
	ld hl, $cab0
	xor a
	ld [$ffc4], a
	ld [$ff9d], a
	call ScrFadeIn
	call DelayFrame
	call $4d06
	ld a, [$ff9d]
	inc a
	ld [$ff9d], a
	ld a, [$ffbf]
	and a
	jr nz, .asm_1dc118
	call $523f
	call $4b70
	call $4bf8
	jp $40fc
.asm_1dc118
	ld bc, $cab0
	xor a
	ld [$ffc4], a
	call $92e
	call $9d2
	call $5289
	call $5297
	ld a, $6d
	call $2be2
	xor a
	ld [$ffbf], a
	ld [$ffae], a
	ld [$ffaf], a
	ld [$ffb0], a
	ld [$ffb1], a
	ld [$dcf3], a
	ld [$dcf4], a
	ld [$dcfb], a
	ld [$dce8], a
	ld [$dcf5], a
	call $4943
	call $52a6
	ld a, $e7
	ld [$ff40], a
	ld hl, $cab0
	xor a
	ld [$ffc4], a
	ld [$ff9d], a
	call ScrFadeIn
	call DelayFrame
	call $4e10
	ld a, [$ff9d]
	inc a
	ld [$ff9d], a
	ld a, [$ffbf]
	and a
	jr nz, .asm_1dc18a
	call $523f
	ld a, [$dcf5]
	cp $01
	jr z, .asm_1dc196
	cp $02
	jr z, .asm_1dc1a2
	cp $03
	jr z, .asm_1dc1f1
	cp $04
	jr z, .asm_1dc19c
	call $4856
	jp $415e
.asm_1dc18a
	ld bc, $cab0
	xor a
	ld [$ffc4], a
	call $96a
	jp $41fc
.asm_1dc196
	call $48d9
	jp $415e
.asm_1dc19c
	call $48bc
	jp $415e
.asm_1dc1a2
	ld a, [$dce8]
	inc a
	ld [$dce8], a
	cp $08
	jr c, .asm_1dc1be
	xor a
	ld [$dce8], a
	ld a, $01
	ld [$ffbf], a
	ld c, $40
	call $5282
	jp $415e
.asm_1dc1be
	ld c, $40
	call $5282
	ld a, $00
	ld [$dcf5], a
	ld bc, $cab0
	xor a
	ld [$ffc4], a
	call $96a
	call $9d2
	call $52a6
	xor a
	ld [$ffbf], a
	ld [$ffae], a
	ld [$ffaf], a
	ld [$ffb0], a
	ld [$ffb1], a
	ld [$dcf3], a
	ld [$dcf4], a
	ld [$dcfb], a
	ld [$dcf5], a
	jp $4149
.asm_1dc1f1
	call $4943
	ld a, $01
	ld [$dcf5], a
	jp $415e
; 0x1dc1fc

SECTION "Intro_GFXa", ROMX[$56DF], BANK[$77]
IntroGFX00::
INCBIN "gfx/intro_00.2bpp"

;SECTION "Intro_GFXa", ROMX[$5680], BANK[$79]
;IntroGFX01:
;INCBIN "gfx/intro_01.2bpp"

SECTION "Intro_GFXb", ROMX[$5577], BANK[$77]
IntroMap00::
	rept 143
	db $00
	endr
	;  a  n     a  d  v  e  n  t  u  r  e
	db 01,07,00,01,03,$d,04,07,$b,$c,09,04
	rept 5+20+1
	db $00
	endr
	;  i  s  x  a  b  o  u  t  x  t  o  x  b  e  g  i  n
	db 06,$a,00,01,02,08,$c,$b,00,$b,08,00,02,04,05,06,07,$e,$e
	
