SECTION "OpeningScripts", ROMX[$4000], BANK[$8]
BallotsHome_Script_00:
	scr_face 1
	scr_text BallotsHome_Text_00	;4542
	scr_face 2
	scr_unkn $3c, $41
	scr_face 1
	scr_face 3
	scr_unkn $3e, $41
	scr_face 1
	scr_text BallotsHome_Text_01	;4552
	db $15,0,2,2,3,$20				; hearts?
	scr_face 0
	scr_text BallotsHome_Text_02	;456d
	scr_text BallotsHome_Text_03	;458a
	scr_text BallotsHome_Text_04	;45a5
	scr_face 2
	scr_unkn $3c, $41
	scr_unkn $3c, $41
	scr_face 0
	scr_unkn $91, $41
	scr_text BallotsHome_Text_05	;45cc
	db 2,3,7,$15,0,0,2,3,$20		; emoticon
	scr_face 2
	scr_unkn $3c, $41
	scr_unkn $3c, $41
	scr_unkn $3c, $41
	scr_face 0
	scr_unkn $91, $41
	scr_text BallotsHome_Text_06	;45db
	db 2,3,7,$15,0,3,2,3,$20		; ? emoticon
	scr_text BallotsHome_Text_07	;45f0
	scr_unkn $91, $41
	scr_unkn $91, $41
	scr_unkn $91, $41
	scr_unkn $91, $41
; outside
	db $5e, 1
	scr_face 2
	db $c, 1, 8
	scr_face 3
	scr_text BallotsHome_Text_08	; 4651
	scr_text BallotsHome_Text_09	; 465e
	scr_text BallotsHome_Text_08	; 46a4
	scr_face 2
	db $1f							; script end?

SECTION "OpeningScripts_Text", ROMX[$4542], BANK[$8]
BallotsHome_Text_00:
	tx_char 1, 0
;			 rendering dry
	tx	"Alright!"
	tx_para	"All that's"
	tx_line "left is a"
	tx_cont "bit of this!"
	tx_end

BallotsHome_Text_01:
	tx_char 1, 0
;			 rendering dry
	tx	"Let's try"
	tx_line	"it out..."
	tx_end

BallotsHome_Text_02:
	tx_char 1, $2A
;			 rendering dry
	tx	"Perfect!"
	tx_para	"This is"
	tx_line "delicious!"
	tx_cont "In fact.."
	tx_line "it tastes"
	tx_cont "just like how"
	tx_line "my mom used"
	tx_cont "to cook!"
	tx_end

BallotsHome_Text_03:
	tx_char 1, $2B
;			 rendering dry
	tx	"...I really"
	tx_line	"miss her..."
	tx_end

BallotsHome_Text_04:
	tx_char 1, 0
;			 rendering dry
	tx	"Though..."
	tx_para "I gotta look"
	tx_line "forward!"
	tx_cont "Be strong!"
	tx_end

BallotsHome_Text_05:
	tx_char 1, 0
;			 rendering dry
	tx	"Dad! Dinner"
	tx_line	"is ready!"
	tx_end

BallotsHome_Text_06:
	tx_char 1, 0
;			 rendering dry
	tx	"DAAAAAD!"
	tx_end

BallotsHome_Text_07:
	tx_char 1, 0
;			 rendering dry
	tx	"Huh? Is he"
	tx_line	"still in the"
	tx_cont "observatory?"
	tx_para "He doesn't"
	tx_line "usually stay"
	tx_cont "this late..."
	tx_para "I better get"
	tx_line "him before"
	tx_cont "the food gets"
	tx_line "cold..."
	tx_end

BallotsHome_Text_08:
	tx_char 9, 8
	tx		"Woof! Woof!"
	tx_end

BallotsHome_Text_09:
;			 rendering dry
	tx_char 1, 0
	tx		"Aww<comma> are you"
	tx_line	"hungry<comma>"
	tx_cont "Whitey?"
	tx_para "I'm gonna go"
	tx_line "pick up Dad."
	tx_end
