;SECTION "OutText", ROMX[$55a7], BANK[$8]
;	db $e0, 1, 0
;	db $f0
;	db "No!", $ee
;	db "I should be", $ed
;	db "going back to", $ee
;	db "the", $ed, "observatory!", $ee
;	    ;observatory!
;	db "Gotta look", $ed
;	db "for my dad.."
;	db $e2

SECTION "npc1_x", ROMX[$4302], BANK[$8]
;starts at 42fe?
;46fe -> 4744
	dw $4744
SECTION "npc1", ROMX[$4744], BANK[$8]
;46fe -> 4744
	tx_char 0, $13
;			 rendering dry
	tx		"Huh?"
	tx_cont	"Is that for"
	tx_line "your father?"
	tx_para "How nice"
	tx_line "of you!"
	tx_end
	
SECTION "VillageText", ROMX[$40b8], BANK[$c]
	db $e1
	db "You are now in:",$ed
	db "YUAN ER VILLAGE"
	db $e2
