SECTION "DelayFrame", ROM0[$2BF]
DelayFrame:: ; 2bf (0:2bf)
	ld a, [$ff40]
	and a
	ret z
	ld a, [$ff90]
	and a
	jr z, DelayFrame
	xor a
	ld [$ff90], a
	ret
; 0x2cc
