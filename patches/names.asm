SECTION "Names", ROMX[$45FB], BANK[$A]
; list of 92 names, tba tba tba
	dw	.name_none		; 46b3
	dw	.name_player	; 46b8
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_whitey	; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_dad		; .
	dw	.name_greasyguy	; in the fanmade documentation
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	dw	.name_none		; .
	; ... 491c
.name_none
	db $F0,$00,$ed
.name_player
	db "Ｂａｌｌｏｔ:",$ed
.name_whitey
	db "Ｗｈｉｔｅｙ:",$ed
.name_dad
; Sài Yà?
	db "Ｄａｄ:",$ed
.name_greasyguy
	db "Ｙｉ Ｓｕｎ Ｒｅｎ:",$ed

SECTION "NamesFix", ROMX[$42fc], BANK[$A]
	db $a0,$e0,$e1,$e2,$e3,$e4,$e5,$e6,$e7,$e8,$e9,$ea,$eb,$ec
	db $a4,$a4,$f4,$f5,$f6,$f7,$a0,$a0,$a0,$a0,$a0,$a0,$a0,$a0;;;
