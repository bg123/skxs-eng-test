# shi kong xing shou (t+eng)

I haven't cleaned up a ton of this, but this is a crappy attempt at an
English translation of the Shi Kong Xing Shou bootleg game.

Mainly it's just a series of patches and small disassembly of game code that
means almost nothing, but here it is anyway.

## Needed

* Shi Kong Xing Shou ROM as "baserom.gbc" 
(MD5 sum: bc910c865527dd524818597b4b4f7f8a)
* [Sanqui's fork of RGBASM](https://github.com/sanqui/rgbds), for now - it relies
on the overlay feature

## Compile

Run `make` on the directory.

Before that, make sure you set the path where the RGBDS binary is located in
the makefile.

Should compile SKXS_Eng.gb and SKXS_Eng.sym.
